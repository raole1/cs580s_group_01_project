package com.example.security_cam.data;

/**
 * This class is used to store preferences on how to decode images and what to
 * save.
 */
public abstract class Preferences {

    private Preferences() {
    }

    // Which motion detection to use
    public static boolean USE_RGB = true;

    // Which photos to save
    public static boolean SAVE_PREVIOUS = true;
    public static boolean SAVE_ORIGINAL = false;
    public static boolean SAVE_CHANGES = false;

    // Time between saving photos
    public static int PICTURE_DELAY = 3000;
}
