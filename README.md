CS580S - Smart Sensing Devices
group members
Renu Aole B00717320
Prathmesh Arnikar B00710506

Project Description : 

Sensors involved in the project:Camera and Accelerometer
Main goal:
This application will use built-in camera to capture any motion in the surroundings, also 
it will alert if camera is in motion with the help of accelerometer.
Application scenario design: For the usage of this app, the camera must be placed still 
and if it detects any movements in the surroundings, it will take pictures and notify user 
(we will use Image processing to capture the difference). App will also help to detect if camera is 
in motion using the accelerometer sensor. This application is useful to turn a spare android smart mobile phone 
into a security camera.

Developement Tool : Android Studio(gradle script)

References :
https://www.spaceotechnologies.com/integrate-android-accelerometer-detect-shake/
http://mindmeat.blogspot.com/2008/11/java-image-comparison.html
https://cloud.google.com/vision/
https://sisu.ut.ee/imageprocessing/book/1



